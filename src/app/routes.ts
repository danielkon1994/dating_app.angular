import { ListResolve } from './_resolvers/list.resolver';
import { MemberEditResolve } from './_resolvers/member-edit.resolver';
import { MemberEditComponent } from './members/member-edit/member-edit.component';
import { MemberListResolve } from './_resolvers/member-list.resolver';
import { MemberDetailComponent } from './members/member-detail/member-detail.component';
import { MemberListComponent } from './members/member-list/member-list.component';
import { AuthGuard } from './_guards/auth.guard';
import { MessagesComponent } from './messages/messages.component';
import { ListsComponent } from './lists/lists.component';
import { HomeComponent } from './home/home.component';
import { Routes, CanActivate } from '@angular/router';
import { MemberDetailResolve } from './_resolvers/member-detail.resolver';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';

export const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            {path: 'messages', component: MessagesComponent},
            {path: 'lists', component: ListsComponent, resolve: {users: ListResolve}},
            {path: 'member-list', component: MemberListComponent,
                resolve: {users: MemberListResolve}},
            {path: 'member/edit', component: MemberEditComponent,
                resolve: {user: MemberEditResolve}, canDeactivate: [PreventUnsavedChanges]},
            {path: 'members/:id', component: MemberDetailComponent,
                resolve: {user: MemberDetailResolve}},
        ]
    },
    {path: '**', redirectTo: '', pathMatch: 'full' }
]