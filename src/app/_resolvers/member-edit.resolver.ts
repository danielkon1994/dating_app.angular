import { AuthService } from './../_services/auth.service';
import { catchError } from 'rxjs/operators';
import { User } from './../_models/user';
import { AlertifyService } from './../_services/alertify.service';
import { UserService } from './../_services/user.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

@Injectable()
export class MemberEditResolve implements Resolve<User> {
    constructor(private userService: UserService,
        private authService: AuthService,
        private router: Router,
        private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot) {
        const token = localStorage.getItem('token');
        return this.userService.getUser(this.authService.jwtService.decodeToken(token).nameid).pipe(
            catchError(error => {
               this.alertify.error('Problem with your data');
               this.router.navigate(['/member-list']);
               return of(null);
            })
        );
    }
}