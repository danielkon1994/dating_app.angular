import { catchError } from 'rxjs/operators';
import { User } from './../_models/user';
import { AlertifyService } from './../_services/alertify.service';
import { UserService } from './../_services/user.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

@Injectable()
export class MemberDetailResolve implements Resolve<User> {
    constructor(private userService: UserService,
        private router: Router,
        private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.userService.getUser(route.params['id']).pipe(
            catchError(error => {
               this.alertify.error('Problem with user processing');
               this.router.navigate(['/member-list']);
               return of(null);
            })
        );
    }
}