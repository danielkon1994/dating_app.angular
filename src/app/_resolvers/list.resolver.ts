import { catchError } from 'rxjs/operators';
import { User } from './../_models/user';
import { AlertifyService } from './../_services/alertify.service';
import { UserService } from './../_services/user.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

@Injectable()
export class ListResolve implements Resolve<User[]> {
    page = 1;
    pageSize = 5;
    likeParams = 'Likers';

    constructor(private userService: UserService,
        private router: Router,
        private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot) {
        console.log('resolve');
        return this.userService.getUsers(this.page, this.pageSize, null, this.likeParams).pipe(
            catchError(error => {
               this.alertify.error('Problem with user processing');
               this.router.navigate(['/home']);
               return of(null);
            })
        );
    }
}