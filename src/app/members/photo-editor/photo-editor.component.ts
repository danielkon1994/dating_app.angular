import { environment } from './../../../environments/environment';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Photo } from '../../_models/photo';
import { FileUploader } from 'ng2-file-upload';
import { AuthService } from '../../_services/auth.service';
import { UserService } from '../../_services/user.service';
import { AlertifyService } from '../../_services/alertify.service';

@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.css']
})
export class PhotoEditorComponent implements OnInit {

  @Input() photos: Photo[];
  @Output() getMainPhotoChange = new EventEmitter<string>();
  uploader: FileUploader;
  hasBaseDropZoneOver: boolean;
  baseUrl = environment.apiBaseUrl;
  currentMainPhoto: Photo;

  constructor(private authService: AuthService,
  private userService: UserService,
  private alertify: AlertifyService) { }

  ngOnInit() {
    this.initFileUploader();
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  initFileUploader() {
    const token = localStorage.getItem('token');
    const nameId = this.authService.jwtService.decodeToken(token).nameid;
    this.uploader = new FileUploader({
      url: this.baseUrl + '/users/' + nameId + '/photos',
      allowedFileType: ['image'],
      autoUpload: false,
      authToken: 'Bearer ' + token,
      isHTML5: true,
      removeAfterUpload: true,
      maxFileSize: 10 * 1024 * 1024
    });

    this.uploader.onAfterAddingAll = (file) => {
      file.withCredential = false;
    };

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        const res: Photo = JSON.parse(response);
        const photo = {
          url: res.url,
          dateAdded: res.dateAdded,
          id: res.id,
          isMain: res.isMain,
          description: res.description,
        };
        this.photos.push(photo);
        if (photo.isMain) {
          this.authService.changePhotoUrl(photo.url);
          this.authService.currentUser.photoUrl = photo.url;
          localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
        }
      }
    };
  }

  setMainPhoto(photo: Photo) {
    const token = localStorage.getItem('token');
    const nameId = this.authService.jwtService.decodeToken(token).nameid;
    this.userService.setMainPhoto(nameId, photo.id).subscribe(() => {
      this.currentMainPhoto = this.photos.filter(p => p.isMain === true)[0];
      this.currentMainPhoto.isMain = false;
      photo.isMain = true;
      this.authService.changePhotoUrl(photo.url);
      this.authService.currentUser.photoUrl = photo.url;
      localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
      this.alertify.success('Success set to main');
    }, error => {
      this.alertify.error('Error');
    });
  }

  deletePhoto(id: number) {
    this.alertify.confirm('Delete photo', 'Are you sure you want to delete this photo?', () => {
      const token = localStorage.getItem('token');
      const userId = this.authService.jwtService.decodeToken(token).nameid;
      this.userService.deletePhoto(userId, id).subscribe(() => {
        const photoIndex = this.photos.findIndex(i => i.id === id);
        this.photos.splice(photoIndex, 1);
        this.alertify.success('Photo has been removed');
      }, error => {
        this.alertify.error('Failed to delete the photo');
      });
    }, null);
  }
}
