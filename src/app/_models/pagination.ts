export interface Pagination {
    currentPage: number;
    totalItems: number;
    totalPages: number;
    pageSize: number;
}

export class PaginationResult<T> {
    pagination: Pagination;
    result: T;
}
