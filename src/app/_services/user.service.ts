import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../_models/user';
import { PaginationResult } from '../_models/pagination';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  getUsers(page?, pageSize?, userParams?, likeParams?): Observable<PaginationResult<User[]>> {
    const paginationResult: PaginationResult<User[]> = new PaginationResult();
    
    let params = new HttpParams();
    if (!!page && !!pageSize) {
      params = params.append('PageNumber', page);
      params = params.append('PageSize', pageSize);
    }

    if (userParams != null) {
      params = params.append('MinAge', userParams.minAge);
      params = params.append('MaxAge', userParams.maxAge);
      params = params.append('Gender', userParams.gender);
      params = params.append('OrderBy', userParams.orderBy);
    }

    if (likeParams === 'Likers') {
      params = params.append('likers', 'true');
    }

    if (likeParams === 'Likees') {
      params = params.append('likees', 'true');
    }
    
    return this.http.get<User[]>(this.baseUrl + '/users', {observe: 'response', params}).pipe(
      map(response => {
        paginationResult.result = response.body;
        if (response.headers.get('Pagination')) {
          paginationResult.pagination = JSON.parse(response.headers.get('Pagination'));
        }
        return paginationResult;
      })
    );
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + '/users/' + id);
  }

  updateUser(id: number, user: User) {
    return this.http.put(this.baseUrl + '/users/' + id, user);
  }

  setMainPhoto(userId: number, id: number) {
    return this.http.post(this.baseUrl + '/users/' + userId + '/photos/' + id + '/setMain', {});
  }

  deletePhoto(userId: number, id: number) {
    return this.http.delete(this.baseUrl + '/users/' + userId + '/photos/' + id);
  }

  sendLike(userId: number, recipientId: number) {
    return this.http.post(this.baseUrl + '/users/' + userId + '/like/' + recipientId, {});
  }
}
