import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../_models/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userToken: any;
  jwtService = new JwtHelperService();
  currentUser: User;
  photoUrl = new BehaviorSubject<string>('../../assets/user.png');
  currentPhotoUrl = this.photoUrl.asObservable();

  constructor(private http: Http) { }

  changePhotoUrl(photoUrl: string) {
    this.photoUrl.next(photoUrl);
  }

  login(model: User) {
    return this.http.post(`${environment.apiBaseUrl}/auth/login`, model, this.requestHeaders())
      .pipe(
        map((response: Response) => {
          const user = response.json();
          if (user) {
            this.userToken = user.token;
            this.currentUser = user.user;
            localStorage.setItem('token', user.token);
            localStorage.setItem('user', JSON.stringify(user.user));
            this.changePhotoUrl(this.currentUser.photoUrl);
          }
        }),
        catchError(this.handleError)
      );
  }

  register(user: User) {
    return this.http.post(`${environment.apiBaseUrl}/auth/register`, user, this.requestHeaders())
      .pipe(catchError(this.handleError));
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtService.isTokenExpired(token);
  }

  private requestHeaders() {
    const headers = new Headers({'Content-Type': 'application/json'});
    return new RequestOptions({headers: headers});
  }

  private handleError(error: any) {
    const applicationError = error.headers.get('Application-Error');
    if (applicationError) {
      return Observable.throw(applicationError);
    }
    const serverError = error.json();
    let modelStateError = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateError += serverError[key];
        }
      }
    }
    return Observable.throw(
      modelStateError || 'Server error'
    );
  }
}
